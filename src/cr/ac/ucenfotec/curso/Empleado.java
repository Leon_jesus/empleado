package cr.ac.ucenfotec.curso;

public class Empleado {

    private String cedula;
    private String nombre;
    private String puesto;

    public Empleado(String cedula, String nombre, String puesto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.puesto = puesto;
    }

    public Empleado() {
        this.puesto = "";
        this.nombre = "";
        this.puesto = "";
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "cedula='" + cedula + '\'' +
                ", nombre='" + nombre + '\'' +
                ", puesto='" + puesto + '\'' +
                '}';
    }
}
