package cr.ac.ucenfotec.curso;

import java.io.PrintStream;
import java.util.Scanner;

public class Main {
    private static PrintStream output = new PrintStream(System.out);
    private static Scanner input = new Scanner(System.in);
    static Empleado [] listaEmpleados = new Empleado[10];

    public static void main(String[] args) {
    menu();

    }

    private static void menu() {
        int opcion=0;
        do {
            System.out.println("Bienvenido al sistema");
            System.out.println("1. Registrar");
            System.out.println("2.Listar Empleados");
            System.out.println("3.Salir");
            System.out.println("Ingrese una opcion");
            opcion = input.nextInt();
            procesarOpcion(opcion);
        } while(opcion!= 3);

        System.out.println();
    }

    private static void procesarOpcion(int opcion) {

        switch (opcion){
            case 1 : registrarEmpleado();
            break;
            case 2 : listarEmpleados();
            break;
            case 3 :
                System.out.println("Ha salido del Sistema");
                System.exit(0);
            default:
                System.out.println("Opcion invalida");
        }
    }

    private static void listarEmpleados() {
        for (int i = 0; i < listaEmpleados.length; i++) {
            if (listaEmpleados[i] != null) {
                System.out.println(listaEmpleados[i].toString());
            }
        }
    }

    private static void registrarEmpleado() {
        System.out.println(" Ingrese la Cedula ");
        String cedula = input.next();
        System.out.println(" Ingrese el Nombre ");
        String nombre = input.next();
        System.out.println(" Ingrese el puesto ");
        String puesto = input.next();

        Empleado empleado = new Empleado(cedula,nombre,puesto);

        for (int i = 0; i < listaEmpleados.length; i++) {
            if(listaEmpleados[i] == null) {
                listaEmpleados[i] = empleado;
                i = listaEmpleados.length;
            }
        }
    }
}
